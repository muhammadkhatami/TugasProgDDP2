import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

import javari.park.Attractions;
import javari.park.Registration;
import javari.park.Section;
import javari.park.Visitor;
import javari.reader.AttractionsReader;
import javari.reader.CategoriesReader;
import javari.reader.RecordsReader;
import javari.reader.CsvReader;

public class A3Festival {

    static CsvReader categoriesAdd;
    static CsvReader attractionAdd;
    static CsvReader recordsAdd;
    static Scanner input = new Scanner(System.in);
    private static Registration visitor;

    public static void main(String[] args) {
        System.out.print("Welcome to Javari Park Festival - Registration Service!"
                + "\n\n... Opening default section database from data.");
        String address = System.getProperty("user.dir") + "\\data";

        while (true) {
            try {
                categoriesAdd = new CategoriesReader(Paths.get(address, "animals_categories.csv"));
                attractionAdd = new AttractionsReader(Paths.get(address, "animals_attractions.csv"));
                recordsAdd = new RecordsReader(Paths.get(address, "animals_records.csv"));
                System.out.println("\n... Loading... Success... System is populating data...\n");
                break;
            } catch (IOException ex) {
                System.out.print("... File not found or incorrect file!"
                        + "\n\nPlease provide the source data path: ");
                address = input.next();
                address = address.replace("\\", "\\\\");
            }
        }

        long categoriesValid = categoriesAdd.countValidRecords();
        long categoriesInvalid = categoriesAdd.countInvalidRecords();
        long attractionValid = attractionAdd.countValidRecords();
        long attractionInvalid = attractionAdd.countInvalidRecords();
        long recordsValid = recordsAdd.countValidRecords();
        long recordsInvalid = recordsAdd.countInvalidRecords();

        System.out.println("Found _" + categoriesValid + "_ valid sections and " + categoriesInvalid
                + " invalid sections");
        System.out.println("Found _" + attractionValid + "_ valid attractions and " + attractionInvalid
                + " invalid attractions");
        System.out.println("Found _" + categoriesValid + "_ valid categories and " + categoriesInvalid
                + " invalid categories");
        System.out.println("Found _" + recordsValid + "_ valid records and " + recordsInvalid
                + " invalid records");

        System.out.print("\nWelcome to Javari Park Festival - Registration Service!"
                + "\n\nPlease answer the questions by typing the number."
                + " Type # if you want to return to the previous menu\n");
        firstMenu(false);
    }

    private static void firstMenu(boolean kondisi){
        System.out.print("\nJavari Park has 3 section:\n"
                + "1. Explore the Mammals\n"
                + "2. World of Aves\n"
                + "3. Reeptilian Kingdom\n"
                + "Please choose your preferred section (type the numbers): ");

        String command = input.nextLine();
        if(!(command.equals("1") || command.equals("2") || command.equals("3"))){
            System.out.println("\nPlease input a correct command");
            firstMenu(kondisi);
        }
        kondisi = secondMenu(command);
        if(kondisi == false){
            firstMenu(kondisi);
        }
        else{
            System.out.println("\nThank you for your interest. Would you like to " +
                    "register to other attractions? (Y/N): ");
            String terima = input.nextLine();
            if(terima.equalsIgnoreCase("Y")){
                firstMenu(false);
            }
        }
    }

    private static boolean secondMenu(String command){
        if(command.equals("1")){
            System.out.println("\n--Explore the Mammals--");
        }
        else if(command.equals("2")){
            System.out.println("\n--World of Aves--");
        }
        else{
            System.out.println("\n--Reptilian Kingdom--");
        }
        ArrayList<String> section = Section.getSection(command);
        for(int i = 0; i < section.size(); i++){
            System.out.println(i + 1 + ". " + section.get(i));
        }
        System.out.print("Please choose your preferred attractions (type the number): ");
        String perintah2 = input.nextLine();
        if(perintah2.equals("#")){
            return false;
        }
        try{
            Integer.parseInt(perintah2);
        }
        catch(NumberFormatException e){
            System.out.println("\nPlease input a correct command");
            return secondMenu(command);
        }
        String performer = null;
        try{
            performer = section.get(Integer.parseInt(perintah2) - 1);
        }
        catch(IndexOutOfBoundsException e){
            System.out.println("\nPlease input a correct command");
            return secondMenu(command);
        }
        return thirdMenu(performer, command);
    }

    private static boolean thirdMenu(String performer, String command){
        System.out.println("\n--" + performer + "--\nAttractions by " + performer + ":");
        ArrayList<Attractions> attractions = Attractions.getAttractions(performer);
        for(int i = 0; i < attractions.size(); i++){
            System.out.println(i + 1 + ". " + attractions.get(i).getName());
        }
        System.out.print("Please choose yout preferred attractions (type the number): ");
        String perintah3 = input.nextLine();
        if(perintah3.equals("#")){
            return secondMenu(command);
        }
        try{
            Integer.parseInt(perintah3);
        }
        catch(NumberFormatException e){
            System.out.println("\nPlease input a correct command");
            return thirdMenu(performer, command);
        }
        Attractions perform = null;
        try{
            perform = attractions.get(Integer.parseInt(perintah3) - 1);
        }
        catch(IndexOutOfBoundsException e){
            System.out.println("\nPlease input a correct command");
            return thirdMenu(performer, command);
        }
        lastCheck(perform, performer, command);
        return true;
    }

    public static boolean lastCheck(Attractions perform, String performa, String perintah){
        System.out.println("\nWow, one more step, \nplease let us know your name: ");
        String name = input.nextLine();
        visitor = Visitor.find(name);
        if(visitor == null){
            visitor = new Visitor(name);
        }
        System.out.print("\nYeay, final check!\nHere is your data, and the attraction you chose:\n"
                + "Name: " + name + "\nAttractions: " + perform.getName() + " -> " + perform.getType() + "\n" +
                "With: ");
        for(int i = 0; i < perform.getPerformers().size(); i++){
            System.out.print(perform.getPerformers().get(i).getName());
            if(i != perform.getPerformers().size() - 1){
                System.out.print(", ");
            }
            else{
                System.out.println("\n");
            }
        }
        System.out.println("Is the data correct? (Y/N): ");
        String confirmation = input.nextLine();
        if(confirmation.equalsIgnoreCase("N")){
            return lastCheck(perform, performa, perintah);
        }
        else if(confirmation.equals("#")){
            return thirdMenu(performa, perintah);
        }
        else if(!(confirmation.equalsIgnoreCase("Y"))){
            System.out.println("\nPlease input a correct command");
            return lastCheck(perform, performa, perintah);
        }
        else{
            visitor.addSelectedAttraction(perform);
            return true;
        }
    }
}