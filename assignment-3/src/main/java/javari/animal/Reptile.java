package javari.animal;

public class Reptile extends Animal {

    private boolean isTame = false;
    private static String[] listOfReptile = {"Snake"};

    public Reptile(Integer id, String type, String name, Gender gender, double length, double weight, String tamed,
                   Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if (tamed.equals("tamed")) {
            isTame = true;
        }
    }

    public static String[] getListOfReptile() {
        return listOfReptile;
    }

    public boolean specificCondition() {
        return !(isTame);
    }
}