package javari.animal;

public class Mammal extends Animal {

    private static String[] listOfMammals = {"Hamster", "Lion", "Cat", "Whale"};
    private boolean isPregnant = false;

    public Mammal(Integer id, String type, String name, Gender gender, double length, double weight, String pregnant,
                  Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if (pregnant.equalsIgnoreCase("pregnant")) {
            this.isPregnant = true;
        }
    }

    public static String[] getMammal() {
        return listOfMammals;
    }

    public boolean specificCondition() {
        return !(isPregnant) && ((!getType().equals("Lion")) || (getGender().equals(Gender.MALE)));
    }

}