package javari.animal;

public class Aves extends Animal {

    private boolean isLayingEggs = false;
    private static String[] listOfAves = {"Eagle", "Parrot"};

    public Aves(Integer id, String type, String name, Gender gender, double length, double weight, String layedEggs,
                Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if (layedEggs.equalsIgnoreCase("laying eggs")) {
            this.isLayingEggs = true;
        }
    }

    public static String[] getListOfAves() {
        return listOfAves;
    }

    public boolean specificCondition() {
        return !(isLayingEggs);
    }
}