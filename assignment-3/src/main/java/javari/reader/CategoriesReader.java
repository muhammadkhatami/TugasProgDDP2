package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import javari.park.Section;

public class CategoriesReader extends CsvReader {

    private long invalid = 0;
    private long valid = 0;
    private boolean mammal = false;
    private boolean reptile = false;
    private boolean aves = false;

    public CategoriesReader(Path file) throws IOException {
        super(file);
    }

    public ArrayList<String> category = new ArrayList<String>();

    public long countValidRecords(){
        List<String> data = super.getLines();
        for(int i = 0; i < data.size(); i++){
            String[] splitData = data.get(i).split(",");
            if(splitData[2].equalsIgnoreCase("Explore the Mammals")
                    && splitData[1].equalsIgnoreCase("Mammals")
                    && (splitData[0].equalsIgnoreCase("Lion")
                    || splitData[0].equalsIgnoreCase("Whale")
                    || splitData[0].equalsIgnoreCase("Cat")
                    || splitData[0].equalsIgnoreCase("Hamster"))){
                new Section(splitData);
                if(mammal == false){
                    mammal = true;
                    valid += 1;
                }
            }
            else if(splitData[2].equalsIgnoreCase("Reptillian Kingdom")
                    && splitData[1].equalsIgnoreCase("Reptiles")
                    && splitData[0].equalsIgnoreCase("Snake")){
                new Section(splitData);
                if(reptile == false){
                    reptile = true;
                    valid += 1;
                }
            }
            else if(splitData[2].equalsIgnoreCase("World of Aves")
                    && splitData[1].equalsIgnoreCase("Aves")
                    && (splitData[0].equalsIgnoreCase("Parrot") || splitData[0].equalsIgnoreCase("Eagle"))){
                new Section(splitData);
                if(aves == false){
                    aves = true;
                    valid += 1;
                }
            }
            else{
                invalid += 1;
            }
        }
        return valid;
    }

    public long countInvalidRecords(){
        return invalid;
    }
}