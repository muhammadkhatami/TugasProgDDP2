package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import javari.park.Attractions;

public class AttractionsReader extends CsvReader {

    private long invalid = 0;
    private long valid = 0;
    private boolean circle = false;
    private boolean dancing = false;
    private boolean counting = false;
    private boolean coder = false;

    public AttractionsReader(Path file) throws IOException {
        super(file);
    }

    public long countValidRecords(){
        List<String> data = super.getLines();
        System.out.println(data.size());
        for(int i = 0; i < data.size(); i++){
            String[] splitData = data.get(i).split(",");
            if(splitData[1].equalsIgnoreCase("Circles of Fires")
                    && (splitData[0].equalsIgnoreCase("Lion")
                    || splitData[0].equalsIgnoreCase("Whale")
                    || splitData[0].equalsIgnoreCase("Eagle"))){
                new Attractions(splitData[0], splitData[1]);
                if(circle == false){
                    circle = true;
                    valid += 1;
                }
            }
            else if(splitData[1].equalsIgnoreCase("Dancing Animals")
                    && (splitData[0].equalsIgnoreCase("Parrot")
                    || splitData[0].equalsIgnoreCase("Cat")
                    || splitData[0].equalsIgnoreCase("Snake")
                    || splitData[0].equalsIgnoreCase("Hamster"))){
                new Attractions(splitData[0], splitData[1]);
                System.out.print("masuk");
                if(dancing == false){
                    dancing = true;
                    valid += 1;
                }
            }
            else if(splitData[1].equalsIgnoreCase("Counting Masters")
                    && (splitData[0].equalsIgnoreCase("Parrot")
                    || splitData[0].equalsIgnoreCase("Whale") || splitData[0].equalsIgnoreCase("Hamster"))){
                new Attractions(splitData[0], splitData[1]);
                if(counting == false){
                    counting = true;
                    valid += 1;
                }
            }
            else if(splitData[1].equalsIgnoreCase("Passionate Coders")
                    && (splitData[0].equalsIgnoreCase("Cat") || splitData[0].equalsIgnoreCase("Hamster") || splitData[0].equalsIgnoreCase("Snake"))){
                new Attractions(splitData[0], splitData[1]);
                if(coder == false){
                    coder = true;
                    valid += 1;
                }
            }
            else{
                invalid += 1;
            }
        }
        return valid;
    }

    public long countInvalidRecords(){
        return invalid;
    }
}