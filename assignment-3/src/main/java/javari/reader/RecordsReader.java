package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import javari.animal.Animal;
import javari.animal.Aves;
import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.Mammal;
import javari.animal.Reptile;
import javari.park.Attractions;

public class RecordsReader extends CsvReader {

    long valid = 0;
    long invalid = 0;

    public RecordsReader(Path file) throws IOException {
        super(file);
    }

    public long countValidRecords(){
        List<String> data = super.getLines();
        for(int i = 0; i < data.size(); i++){
            String[] splitData = data.get(i).split(",");
            if(splitData[1].equalsIgnoreCase("lion") || splitData[1].equalsIgnoreCase("cat")
                    || splitData[1].equalsIgnoreCase("whale") || splitData[1].equalsIgnoreCase("hamster")){
                addTo(new Mammal(Integer.parseInt(splitData[0]), splitData[1], splitData[2], Gender.parseGender(splitData[3])
                        , Double.parseDouble(splitData[4]), Double.parseDouble(splitData[5]), splitData[6], Condition.parseCondition(splitData[7])));
                valid += 1;

            }
            else if(splitData[1].equalsIgnoreCase("snake")){
                addTo(new Reptile(Integer.parseInt(splitData[0]), splitData[1], splitData[2], Gender.parseGender(splitData[3])
                        , Double.parseDouble(splitData[4]), Double.parseDouble(splitData[5]), splitData[6], Condition.parseCondition(splitData[7])));                valid += 1;
            }
            else if(splitData[1].equalsIgnoreCase("eagle") || splitData[1].equalsIgnoreCase("parrot")){
                addTo(new Aves(Integer.parseInt(splitData[0]), splitData[1], splitData[2], Gender.parseGender(splitData[3])
                        , Double.parseDouble(splitData[4]), Double.parseDouble(splitData[5]), splitData[6], Condition.parseCondition(splitData[7])));                valid += 1;
            }
            else{
                invalid += 1;
            }
        }
        return valid;
    }

    public long countInvalidRecords(){
        return invalid;
    }

    public void addTo(Animal animal){
        List<Attractions> attract = Attractions.getAttractions();
        for(int i = 0; i < attract.size(); i++){
            if(attract.get(i).getType().equalsIgnoreCase(animal.getType())){
                attract.get(i).addPerformer(animal);
            }
        }
    }
}