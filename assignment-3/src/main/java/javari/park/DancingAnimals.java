package javari.park;

import java.util.ArrayList;

public class DancingAnimals extends Attractions {

    private static String[] dancingAnimals = {"Parrot", "Snake", "Cat", "Hamster"};
    private ArrayList<String> animalArrayList = new ArrayList<String>();

    public DancingAnimals(String name, String type) {
        super(name, type);
    }

    public static boolean checkValidity(String animalType) {
        for (String animalCheck : dancingAnimals) {
            if (animalType.equals(animalCheck)) {
                return true;
            }
        }
        return false;
    }
}