package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Visitor implements Registration {

    private String name;
    private int id;
    private int registrationCounter = 0;
    private static ArrayList<Registration> listOfVisitors = new ArrayList<Registration>();
    private ArrayList<SelectedAttraction> listOfSelectedAttractions = new ArrayList<SelectedAttraction>();


    public Visitor(String name) {
        this.name = name;
        this.id = registrationCounter;
        listOfVisitors.add(this);
        registrationCounter += 1;
    }

    public int getRegistrationId() {
        return this.id;
    }

    public String getVisitorName() {
        return this.name;
    }

    public void setVisitorName(String name) {
        this.name = name;
    }

    public List<SelectedAttraction> getSelectedAttractions() {
        return listOfSelectedAttractions;
    }

    public boolean addSelectedAttraction(SelectedAttraction selected) {
        listOfSelectedAttractions.add(selected);
        return true;
    }

    public static Registration find(String name) {
        for (int x = 0; x < listOfVisitors.size(); x++) {
            if (listOfVisitors.get(x).getVisitorName().equalsIgnoreCase(name)) {
                return listOfVisitors.get(x);
            }
        }
        return null;
    }
}