package javari.park;

import javari.animal.*;
import java.util.List;
import java.util.ArrayList;

public class Attractions implements SelectedAttraction{
    private String name;
    private String type;
    private List<Animal> performer = new ArrayList<Animal>();
    private static List<Attractions> attractions = new ArrayList<Attractions>();

    public Attractions(String type, String name){
        this.name = name;
        this.type = type;
        attractions.add(this);
    }

    public String getName(){
        return this.name;
    }

    public String getType(){
        return this.type;
    }

    public List<Animal> getPerformers(){
        return performer;
    }

    public boolean addPerformer(Animal performer){
        if(performer.isShowable()){
            this.performer.add(performer);
            return true;
        }
        return false;
    }

    public static List<Attractions> getAttractions(){
        return attractions;
    }

    public static ArrayList<Attractions> getAttractions(String performa){
        ArrayList<Attractions> newAttractions = new ArrayList<Attractions>();
        for(int i = 0; i < attractions.size(); i++){
            if(attractions.get(i).getType().equalsIgnoreCase(performa)){
                newAttractions.add(attractions.get(i));
            }
        }
        return newAttractions;
    }
}