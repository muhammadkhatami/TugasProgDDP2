package javari.park;

import java.util.ArrayList;

public class PassionateCoder extends Attractions {

    private static String[] passionateCodersAnimal = {"Hamster", "Cat", "Snake"};
    private ArrayList<String> animalArrayList = new ArrayList<String>();

    public PassionateCoder(String name, String type) {
        super(name, type);
    }

    public static boolean checkValidity(String animalType) {
        for (String animalCheck : passionateCodersAnimal) {
            if (animalType.equals(animalCheck)) {
                return true;
            }
        }
        return false;
    }
}