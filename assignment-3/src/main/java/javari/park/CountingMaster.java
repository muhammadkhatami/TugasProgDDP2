package javari.park;

import java.util.ArrayList;

public class CountingMaster extends Attractions {

    private static String[] countingMastersAnimal = {"Hamster", "Whale", "Parrot"};
    private ArrayList<String> animalArrayList = new ArrayList<String>();

    public CountingMaster(String name, String type) {
        super(name, type);
    }

    public static boolean checkValidity(String animalType) {
        for (String animalCheck : countingMastersAnimal) {
            if (animalType.equals(animalCheck)) {
                return true;
            }
        }
        return false;
    }
}