package javari.park;

import java.util.ArrayList;

public class CircleOfFire extends Attractions {

    private static String[] circleOfFireAnimal = {"Lion", "Whale", "Eagle"};
    private ArrayList<String> animalArrayList = new ArrayList<String>();

    public CircleOfFire(String name, String type) {
        super(name, type);
    }

    public static boolean checkValidity(String animalType) {
        for (String animalCheck : circleOfFireAnimal) {
            if (animalType.equals(animalCheck)) {
                return true;
            }
        }
        return false;
    }

}