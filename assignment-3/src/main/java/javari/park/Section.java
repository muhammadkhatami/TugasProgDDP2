package javari.park;

import java.util.ArrayList;
import java.util.Scanner;

public class Section{
    private static ArrayList<String> explore = new ArrayList<String>();
    private static ArrayList<String> world = new ArrayList<String>();
    private static ArrayList<String> kingdom = new ArrayList<String>();

    private static Scanner input = new Scanner(System.in);

    public Section(String[] info){
        if(info[1].equalsIgnoreCase("Mammals")){
            explore.add(info[0]);
        }
        else if(info[1].equalsIgnoreCase("Aves")){
            world.add(info[0]);
        }
        else{
            kingdom.add(info[0]);
        }
    }

    public static ArrayList<String> getSection(String sector){
        if(sector.equals("1")){
            return explore;
        }
        else if(sector.equals("2")){
            return world;
        }
        return kingdom;
    }
}