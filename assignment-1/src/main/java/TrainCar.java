public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    public WildCat cat;
    public TrainCar next;

    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        if (next == null) {
            return cat.weight + EMPTY_WEIGHT;
        }
        return cat.weight + EMPTY_WEIGHT + next.computeTotalWeight();
    }

    public double computeTotalMassIndex() {
        if (next == null) {
            return cat.computeMassIndex();
        }
        return cat.computeMassIndex() + next.computeTotalMassIndex();
    }

    public void printCar() {
        System.out.printf("--(%s) %n",cat.name);
        if (next == null) System.out.println("");
        else {
            next.printCar();
        }
    }
}
