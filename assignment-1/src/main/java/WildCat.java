public class WildCat {

    String name;
    double weight; // In kilograms
    double length; // In centimeters
    final double CM_to_M = 0.01;

    public void setName(String name) {
        this.name = name;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }
    public void setLength(double length) {
        this.weight = weight;
    }

    public String getName() {
        return this.name;
    }
    public double getWeight() {
        return this.weight;
    }
    public double getLength() {
        return this.length;
    }

    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    public double computeMassIndex() {
        double BMI = weight / Math.pow(this.length * CM_to_M, 2);
        return BMI;
    }
}
