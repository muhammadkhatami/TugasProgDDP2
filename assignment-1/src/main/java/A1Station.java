//Muhammad Khatami - 1706044055

import java.util.ArrayList;
import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    private static ArrayList<TrainCar> train = new ArrayList<TrainCar>();

    public static void main (String [] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("input the number of cat : ");
        int numberOfCat = Integer.parseInt(input.nextLine());

        for(int i = 0; i<numberOfCat; i++) {
            System.out.print("input cat data : ");
            String[] catData = input.nextLine().split(",");
            String catName = catData[0];
            double catWeight = Double.parseDouble(catData[1]);
            double catLength = Double.parseDouble(catData[2]);

            WildCat cat = new WildCat(catName, catWeight, catLength);

            if(train.size() == 0) {
                train.add(new TrainCar(cat));
            }else{
                train.add(new TrainCar(cat, train.get(train.size()-1)));
            }

            if(train.get(train.size()-1).computeTotalWeight() > THRESHOLD){
                System.out.println("The train departs to Javari Park!");
                System.out.print("[LOCO]<--");
                train.get(train.size()-1).printCar();
                System.out.println("Average mass index of all cats: " + String.format("%.2f", train.get(train.size()-1).computeTotalMassIndex()/train.size()));
                if(train.get(train.size()-1).computeTotalMassIndex() < 18.5){
                    System.out.println("In average, the cats in the train are *underweight*\n");
                }
                else if(train.get(train.size()-1).computeTotalMassIndex()/train.size() < 25 && train.get(train.size()-1).computeTotalMassIndex()/train.size() >= 18){
                    System.out.println("In average, the cats in the train are *normal*\n");
                }
                else if(train.get(train.size()-1).computeTotalMassIndex()/train.size() < 30 && train.get(train.size()-1).computeTotalMassIndex()/train.size() >= 25){
                    System.out.println("In average, the cats in the train are *overweight*\n");
                }
                else{
                    System.out.println("In average, the cats in the train are *obese*\n");
                }
                train.clear();
            }else if(i == numberOfCat-1) {
                System.out.println("The train departs to Javari Park!");
                System.out.print("[Loco]<--");
                train.get(train.size()-1).printCar();
                System.out.println("Average mass index of all cats: " + String.format("%.2f", train.get(train.size()-1).computeTotalMassIndex()/train.size()));
                //Menentukan tipe berat WildCat
                if(train.get(train.size()-1).computeTotalMassIndex() < 18.5){
                    System.out.println("In average, the cats in the train are *underweight*");
                }
                else if(train.get(train.size()-1).computeTotalMassIndex()/train.size() < 25 && train.get(train.size()-1).computeTotalMassIndex()/train.size() >= 18){
                    System.out.println("In average, the cats in the train are *normal*");
                }
                else if(train.get(train.size()-1).computeTotalMassIndex()/train.size() < 30 && train.get(train.size()-1).computeTotalMassIndex()/train.size() >= 25){
                    System.out.println("In average, the cats in the train are *overweight*");
                }
                else if(train.get(train.size()-1).computeTotalMassIndex()/train.size() > 30){
                    System.out.println("In average, the cats in the train are *obese*");
                }
            }
        }
    }
}
