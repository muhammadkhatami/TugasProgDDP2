package main.java.cage;

import java.util.ArrayList;
import main.java.animal.Animal;

public class Cage {
    private ArrayList<Animal> level1 = new ArrayList<Animal>();
    private ArrayList<Animal> level2 = new ArrayList<Animal>();
    private ArrayList<Animal> level3 = new ArrayList<Animal>();
    private ArrayList<Animal> animalArrayList;
    private boolean haveArrange = false;

    public Cage(ArrayList<Animal> animalArrayList) {
        this.animalArrayList = animalArrayList;
    }

    public void cagesProgIndoor() {
        System.out.println("location: indoor");
        beforeArrange(animalArrayList);
        printCage();
        afterArrange();
        printCage();
    }

    public void cagesProgOutdoor() {
        System.out.println("location: outdoor");
        beforeArrange(animalArrayList);
        printCage();
        afterArrange();
        printCage();
    }

    private void beforeArrange(ArrayList<Animal> animalArrayList) {
        if (animalArrayList.size() >= 3) {
            int jumlahPerLevel = animalArrayList.size() / 3;
            for (int i = 0; i < animalArrayList.size(); i++) {
                if (this.level1.size() < jumlahPerLevel) {
                    this.level1.add(animalArrayList.get(i));
                } else if (this.level2.size() < jumlahPerLevel) {
                    this.level2.add(animalArrayList.get(i));
                } else {
                    this.level3.add(animalArrayList.get(i));
                }
            }
        } else if (animalArrayList.size() == 1) {
            this.level1.add(animalArrayList.get(0));
        } else if (animalArrayList.size() == 2) {
            this.level1.add(animalArrayList.get(0));
            this.level2.add(animalArrayList.get(1));
        }
    }

    private void reverseInside(ArrayList<Animal> binatangListLev) {
        for (int i = 0; i < (binatangListLev.size() / 2); i++) {
            Animal temp = binatangListLev.get(i);
            binatangListLev.set(i, binatangListLev.get(binatangListLev.size() - (i + 1)));
            binatangListLev.set(binatangListLev.size() - (i + 1),temp);
        }
        haveArrange = true;
    }

    private void afterArrange() {
        reverseInside(this.level1);
        reverseInside(this.level2);
        reverseInside(this.level3);
    }

    private void printCage() {
        if (!haveArrange) {
            System.out.print("evel 3: ");
            for (int i = 0; i < this.level3.size(); i++) {
                System.out.print(this.level3.get(i).getName() + " ("
                        + this.level3.get(i).getLength() + " - "
                        + this.level3.get(i).getCageCode() + ")" + ", ");
            }
            System.out.println();
            System.out.print("level 2: ");
            for (int i = 0; i < this.level2.size(); i++) {
                System.out.print(this.level2.get(i).getName()
                        + " (" + this.level2.get(i).getLength() + " - "
                        + this.level2.get(i).getCageCode() + ")" + ", ");
            }
            System.out.println();
            System.out.print("level 1: ");
            for (int i = 0; i < this.level1.size(); i++) {
                System.out.print(this.level1.get(i).getName() + " ("
                        + this.level1.get(i).getLength() + " - "
                        + this.level1.get(i).getCageCode() + ")" + ", ");
            }
            System.out.println();
            System.out.println();
        } else {
            System.out.println("After rearrangement...");
            System.out.print("level 3: ");
            for (int i = 0; i < this.level2.size(); i++) {
                System.out.print(this.level2.get(i).getName()
                        + " (" + this.level2.get(i).getLength() + " - "
                        + this.level2.get(i).getCageCode() + ")" + ", ");
            }
            System.out.println();
            System.out.print("level 2: ");
            for (int i = 0; i < this.level1.size(); i++) {
                System.out.print(this.level1.get(i).getName() + " ("
                        + this.level1.get(i).getLength() + " - "
                        + this.level1.get(i).getCageCode() + ")" + ", ");
            }
            System.out.println();
            System.out.print("level 1: ");
            for (int i = 0; i < this.level3.size(); i++) {
                System.out.print(this.level3.get(i).getName() + " ("
                        + this.level3.get(i).getLength() + " - "
                        + this.level3.get(i).getCageCode() + ")" + ", ");
            }
            System.out.println();
            System.out.println();
        }
    }
}
