package main.java;

import java.util.ArrayList;
import java.util.Scanner;
import main.java.animal.Animal;
import main.java.animal.pet.Cat;
import main.java.animal.pet.Hamsters;
import main.java.animal.pet.Parrot;
import main.java.animal.wild.Eagle;
import main.java.animal.wild.Lion;
import main.java.cage.Cage;

public class JavariMainProg {
    private static ArrayList<Animal> catList = new ArrayList<>();
    private static ArrayList<Animal> eagleList = new ArrayList<>();
    private static ArrayList<Animal> hamsterList = new ArrayList<>();
    private static ArrayList<Animal> parrotList = new ArrayList<>();
    private static ArrayList<Animal> lionList = new ArrayList<>();

    public static void main(String[] args) {
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");
        Scanner input = new Scanner(System.in);

        System.out.print("cat: ");
        int catTotal = input.nextInt();
        if (catTotal > 0) {
            System.out.println("Provide the information of cat(s):");
            String[] catData = input.next().replace("|", " ").replace(",", " ")
                    .split(" ");
            for (int i = 0; i < catData.length; i += 2) {
                Animal cat = new Cat(catData[i], Integer.parseInt(catData[i + 1]));
                catList.add(cat);
            }
        }

        System.out.print("Lion: ");
        int lionTotal = input.nextInt();
        if (lionTotal > 0) {
            System.out.println("Provide the information of cat(s):");
            String[] lionData = input.next().replace("|", " ").replace(",", " ")
                    .split(" ");
            for (int i = 0; i < lionData.length; i += 2) {
                Animal lion = new Lion(lionData[i], Integer.parseInt(lionData[i + 1]));
                lionList.add(lion);
            }
        }

        System.out.print("Eagle: ");
        int eagleTotal = input.nextInt();
        if (eagleTotal > 0) {
            System.out.println("Provide the information of cat(s):");
            String[] eagleData = input.next().replace("|", " ").replace(",", " ")
                    .split(" ");
            for (int i = 0; i < eagleData.length; i += 2) {
                Animal eagle = new Eagle(eagleData[i], Integer.parseInt(eagleData[i + 1]));
                eagleList.add(eagle);
            }
        }

        System.out.print("Parrot: ");
        int parrotTotal = input.nextInt();
        if (parrotTotal > 0) {
            System.out.println("Provide the information of cat(s):");
            String[] parrotData = input.next().replace("|", " ").replace(",", " ")
                    .split(" ");
            for (int i = 0; i < parrotData.length; i += 2) {
                Animal parrot = new Parrot(parrotData[i], Integer.parseInt(parrotData[i + 1]));
                parrotList.add(parrot);
            }
        }

        System.out.print("Hamster: ");
        int hamsterTotal = input.nextInt();
        if (hamsterTotal > 0) {
            System.out.println("Provide the information of cat(s):");
            String[] hamsterData = input.next().replace("|", " ").replace(",", " ")
                    .split(" ");
            for (int i = 0; i < hamsterData.length; i += 2) {
                Animal hamster = new Hamsters(hamsterData[i], Integer.parseInt(hamsterData[i + 1]));
                hamsterList.add(hamster);
            }
        }

        System.out.println("Animals have been successfully recorded!\n\n================"
                + "=============================\n" + "Cage arrangement:");

        if (!catList.isEmpty()) {
            Cage catCage = new Cage(catList);
            catCage.cagesProgIndoor();
        }
        if (!lionList.isEmpty()) {
            Cage lionCage = new Cage(lionList);
            lionCage.cagesProgOutdoor();
        }
        if (!eagleList.isEmpty()) {
            Cage eagleCage = new Cage(eagleList);
            eagleCage.cagesProgOutdoor();
        }
        if (!parrotList.isEmpty()) {
            Cage parrotCage = new Cage(parrotList);
            parrotCage.cagesProgIndoor();
        }
        if (!hamsterList.isEmpty()) {
            Cage hamsterCage = new Cage(hamsterList);
            hamsterCage.cagesProgIndoor();
        }

        System.out.println("NUMBER OF ANIMALS: ");
        System.out.println("cat:" + String.valueOf(catTotal));
        System.out.println("lion:" + String.valueOf(lionTotal));
        System.out.println("eagle:" + String.valueOf(eagleTotal));
        System.out.println("parrot:" + String.valueOf(parrotTotal));
        System.out.println("hamster:" + String.valueOf(hamsterTotal));
        System.out.println("\n=============================================");
        JavariMainProg park = new JavariMainProg();
        park.visitTheAnimal();
    }

    public void visitTheAnimal() {
        while (true) {
            System.out.print("Which animal you want to visit?\n"
                    + "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)\n");
            Scanner input = new Scanner(System.in);
            String animalNumber = input.nextLine();
            boolean animalAvaibility = false;
            if (animalNumber.equalsIgnoreCase("1")) {
                System.out.println("Mention te name of cat you want to visit: ");
                String animalName = input.nextLine();
                for (Animal tempAnimal : catList) {
                    if (animalName.equalsIgnoreCase(tempAnimal.getName())) {
                        System.out.print("You are visiting " + animalName
                                + " (cat) now, what would you like"
                                + " to do?\n" + "1: Brush the fur 2: Cuddle\n");
                        int tempNumber = Integer.parseInt(input.nextLine());
                        tempAnimal.doIt(tempNumber);
                        animalAvaibility = true;
                    }
                }
                if (!animalAvaibility) {
                    System.out.println("There is no cat with that name! Back to the office!\n");
                }
            } else if (animalNumber.equalsIgnoreCase("2")) {
                System.out.print("Mention the name of eagle you want to visit: ");
                String animalName = input.nextLine();
                for (Animal tempAnimal : eagleList) {
                    if (animalName.equalsIgnoreCase(tempAnimal.getName())) {
                        System.out.print("You are visiting " + animalName
                                + " (eagle) now, what would you like "
                                + "to do?\n" + "1: Order to fly");
                        int tempNumber = Integer.parseInt(input.nextLine());
                        tempAnimal.doIt(tempNumber);
                        animalAvaibility = true;
                    }
                }
                if (!animalAvaibility) {
                    System.out.println("There is no eagle with that name! ");
                }
            } else if (animalNumber.equalsIgnoreCase("3")) {
                System.out.println("Mention the name of Hamster you want to visit: ");
                String animalName = input.nextLine();
                for (Animal tempAnimal : hamsterList) {
                    if (animalName.equalsIgnoreCase(tempAnimal.getName())) {
                        System.out.print("You are visiting " + animalName
                                + " (hamster) now, what would you like "
                                + "to do?\n"
                                + "1: See it gnawing 2: Order to run in the hamster wheel");
                        int tempNumber = Integer.parseInt(input.nextLine());
                        tempAnimal.doIt(tempNumber);
                        animalAvaibility = true;
                    }
                }
                if (!animalAvaibility) {
                    System.out.println("There is no hamster with that name! ");
                }
            } else if (animalNumber.equalsIgnoreCase("4")) {
                System.out.println("Mention the name of Parrot you want to visit: ");
                String animalName = input.nextLine();
                for (Animal tempAnimal : parrotList) {
                    if (animalName.equalsIgnoreCase(tempAnimal.getName())) {
                        System.out.print("You are visiting " + animalName
                                + " (parrot) now, what would you like "
                                + "to do?\n" + "1: Order to fly 2: Do conversation");
                        int tempNumber = Integer.parseInt(input.nextLine());
                        tempAnimal.doIt(tempNumber);
                        animalAvaibility = true;
                    }
                }
                if (!animalAvaibility) {
                    System.out.println("There is no parrot with that name! ");
                }
            } else if (animalNumber.equalsIgnoreCase("5")) {
                System.out.println("Mention the name of Lion you want to visit: ");
                String animalName = input.nextLine();
                for (Animal tempAnimal : lionList) {
                    if (animalName.equalsIgnoreCase(tempAnimal.getName())) {
                        System.out.print("You are visiting " + animalName
                                + " (lion) now, what would you like "
                                + "to do?\n" + "1: See it hunting 2: Brush the mane 3: Disturb it");
                        int tempNumber = Integer.parseInt(input.nextLine());
                        tempAnimal.doIt(tempNumber);
                        animalAvaibility = true;
                    }
                }
                if (!animalAvaibility) {
                    System.out.println("There is no lion with that name! ");
                }
            } else {
                break;
            }
        }
    }
}