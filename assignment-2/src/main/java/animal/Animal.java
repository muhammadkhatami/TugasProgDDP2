package main.java.animal;

public class Animal {
    private String name;
    private int length;
    private char cageCode;
    private String place;

    public Animal(String name, int length) {
        this.name = name;
        this.length = length;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    public void setCageCode(char cageCode) {
        this.cageCode = cageCode;
    }

    public char getCageCode() {
        return cageCode;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPlace() {
        return place;
    }

    public void doIt(int number) {

    }

}
