package main.java.animal.wild;

import main.java.animal.Animal;

public class Wild extends Animal {
    public Wild(String name, int length) {
        super(name, length);
        this.setPlace("outdoor");

        if (this.getLength() < 75) {
            this.setCageCode('A');
        } else if (this.getLength() >= 75 && this.getLength() <= 90) {
            this.setCageCode('B');
        } else {
            this.setCageCode('C');
        }
    }
}
