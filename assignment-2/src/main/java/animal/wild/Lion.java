package main.java.animal.wild;

public class Lion extends Wild {
    public Lion(String name, int length) {
        super(name, length);
    }

    public void doIt(int number) {
        if (number == 1) {
            this.seeItHunting();
        } else if (number == 2) {
            this.brushTheMane();
        } else if (number == 3) {
            this.disturbIt();
        } else {
            System.out.println(this.getName() + " says: HM?");
        }
    }

    public void seeItHunting() {
        System.out.println("Lion is hunting..\n" + this.getName() + " makes a voice: err...!\n");
    }

    public void brushTheMane() {
        System.out.println("Clean the lion’s mane.." + this.getName()
                + " makes a voice: Hauhhmm!\n");
    }

    public void disturbIt() {
        System.out.println(this.getName() + " makes a voice: HAUHHMM!\n");
    }
}
