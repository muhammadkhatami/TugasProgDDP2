package main.java.animal.wild;

public class Eagle extends Wild {
    public Eagle(String name, int length) {
        super(name, length);
    }

    public void doIt(int number) {
        if (number == 1) {
            this.orderToFly();
        } else {
            System.out.println("You do nothing...\n" + "Back to the office!\n");
        }
    }

    public void orderToFly() {
        System.out.println(this.getName() + "makes a voice: kwaakk...\n"
                + "You hurt!\n" + "Back to the office!\n");
    }
}
