package main.java.animal.pet;

import java.util.Scanner;

public class Parrot extends Pet {
    public Parrot(String name, int length) {
        super(name, length);
    }

    public void doIt(int number) {
        if (number == 1) {
            this.orderToFly();
        } else if (number == 2) {
            this.doConversation();
        } else {
            System.out.println(this.getName() + " says: HM?\n");
        }
    }

    public void orderToFly() {
        System.out.println("Parrot" + this.getName() + " flies!\n"
                + this.getName() + " makes a voice: FLYYYY…..\n");
    }

    public void doConversation() {
        Scanner conversationInput = new Scanner(System.in);
        System.out.print("You say: " + conversationInput.nextLine());
        System.out.println(this.getName() + " says: HELLO CUTE BIRD..\n");
    }
}