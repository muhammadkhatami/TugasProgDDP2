package main.java.animal.pet;

public class Cat extends Pet {
    public Cat(String name, int length) {
        super(name, length);
    }

    public void doIt(int number) {
        if (number == 1) {
            this.brushTheFur();
        } else if (number == 2) {
            this.cuddle();
        } else {
            System.out.println("You do nothing...\n" + "Back to the office!\n");
        }
    }

    public void brushTheFur() {
        System.out.println("Time to clean " + this.getName() + "'s fur\n"
                + this.getName() + "makes a voice: Nyaaan...\n"
                + "Back to the office!\n");
    }

    public void cuddle() {
        System.out.println(this.getName() + " makes a voice: Purrr..\n"
                + "Back to the office!\n");
    }
}

