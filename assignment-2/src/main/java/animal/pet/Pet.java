package main.java.animal.pet;

import main.java.animal.Animal;

public class Pet extends Animal {
    public Pet(String name, int length) {
        super(name, length);
        this.setPlace("indoor");

        if (this.getLength() < 45) {
            this.setCageCode('A');
        } else if (this.getLength() >= 45 && this.getLength() < 60) {
            this.setCageCode('B');
        } else {
            this.setCageCode('C');
        }
    }
}
