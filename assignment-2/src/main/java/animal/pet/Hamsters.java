package main.java.animal.pet;

public class Hamsters extends Pet {
    public Hamsters(String name, int length) {
        super(name, length);
    }

    public void doIt(int number) {
        if (number == 1) {
            this.seeItGnawing();
        } else if (number == 2) {
            this.runInTheHamsterWheel();
        } else {
            System.out.println("You do nothing...");
        }
    }

    public void seeItGnawing() {
        System.out.println(this.getName() + " makes a voice: ngkkrit.. ngkkrrriiit\n");
    }

    public void runInTheHamsterWheel() {
        System.out.println(this.getName() + " makes a voice: trrr…. trrr...\n");
    }
}
