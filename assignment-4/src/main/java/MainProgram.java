import board.Board;

import javax.swing.JFrame;
import java.io.IOException;


public class MainProgram{
    public static void main(String[] args) throws IOException {

        JFrame frame = new JFrame("Matching Game by Muhammad Khatami"); //set title

        frame.setSize(630,980); //set frame size
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Board board = new Board(frame);
        frame.setVisible(true);
    }
}