package board;

import card.Card;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.imageio.ImageIO;
import javax.swing.*;

public class Board {

    private ArrayList<Integer> added = new ArrayList<Integer>();
    private ArrayList<Card> cards = new ArrayList<Card>();
    public static JFrame frame;

    public Board(JFrame frame) throws IOException {
        //membuat frame
        this.frame = frame;
        frame.setLayout(new GridLayout(7, 6)); //set frame layout

        for(int i = 1; i <= 18; i++){
            for(int j = 0; j < 2; j++){
                added.add(i);
            }
        }
        //shuffle
        Collections.shuffle(added);

        //membuat kartu pada class Card
        int count = 0;
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 6; j++){
                Card card = new Card(String.valueOf(added.get(count))); //membuat setiap kartu
                frame.add(card.getCard());
                cards.add(card);
                count += 1;
            }
        }

        makeTriesCounter(); //memanggil method membuat label tries counter
        makeCloseButton(); //memanggil method membuat tombol close
        makeNewGameButton(); //memanggil method membuat newGame Button
    }

    public void makeTriesCounter() {
        frame.add(new JLabel("Number of tries")); //set Judul label
        JLabel tries = Card.getTries(); //mengambil nilai counter
        frame.add(tries); //memasukan label ke frame
    }

    public void makeCloseButton() {
        JButton closeButton = new JButton(); //membuat close button
        try {
            //memuat gambar untuk close button
            BufferedImage img = ImageIO.read(new File("D:\\UI\\Semester 2\\DDP-2\\TugasProgDDP2\\" +
                    "assignment-4\\images\\close.jpg"));
            closeButton.setIcon(new ImageIcon(img));
            frame.add(closeButton);
        } catch (IOException e) {
            System.out.println(e);
        }
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
            }
        });
        frame.add(closeButton);
    }

    public void makeNewGameButton() {
        JButton newGameButton = new JButton(); //membuat newGame Button
        //memuat gambar untuk new game button
        try {
            BufferedImage img = ImageIO.read(new File("D:\\UI\\Semester 2\\DDP-2\\TugasProgDDP2\\" +
                    "assignment-4\\images\\newgame.jpg"));
            newGameButton.setIcon(new ImageIcon(img));
            frame.add(newGameButton);
        } catch (IOException e) {
            System.out.println(e);
        }
        //saat tombol di klik, akan mereset ulang game.
        newGameButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Collections.shuffle(added);
                for(int i = 0; i < cards.size(); i++){
                    cards.get(i).setIcon(cards.get(i).getCardCover());
                    cards.get(i).reset(String.valueOf(added.get(i)));
                }
                Card.resetAll();
            }
        });
    }
}
