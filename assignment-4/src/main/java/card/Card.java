package card;

import board.Board;


import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Card extends JButton{

    private JButton card;
    private ImageIcon cardPicture;
    private ImageIcon cardCover;
    private String cardId;
    private boolean clicked = false;
    private boolean matched = false;
    private static int matchedCounter;

    private static JLabel tries;
    private static int numOfTries = 0;
    private static ArrayList<Card> opened = new ArrayList<Card>();
    private static ArrayList<Card> cardArrayList = new ArrayList<Card>();


    private Timer timer = new Timer(500, new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            check();
        }
    });

    public ImageIcon getCardCover() {
        return cardCover;
    }

    public void setMatched(boolean matched) {
        this.matched = matched;
    }

    public boolean isMatched() {
        return matched;
    }

    private void check() {
        //method untuk check match atau ngga nya kartu yang di klik
        if(opened.get(0).getCardId().equals(opened.get(1).getCardId())){
            opened.get(0).getCard().setEnabled(false);
            opened.get(1).getCard().setEnabled(false);
            opened.get(1).setMatched(true);
            opened.get(0).setMatched(true);
            matchedCounter++;
        } else {
            opened.get(0).getCard().setIcon(cardCover);
            opened.get(0).setClicked(false);
            opened.get(1).getCard().setIcon(cardCover);
            opened.get(1).setClicked(false);
        }
        opened.clear();
        if (matchedCounter == 18) {
            winChecker();
        }
    }

    private void winChecker() {
        //Pada saat user sudah menyocokan semua kartu, akan muncul pop up
        JOptionPane.showMessageDialog(Board.frame, "SELAMAT ANDA MENANG\nANDA MEMANG LUAR BIASA",
                "KEMENANGAN MILIK KITA", JOptionPane.INFORMATION_MESSAGE);
    }

    public Card(String cardId) {
        //load kartu
        this.card = new JButton();
        this.cardId = cardId;
        loadCardImage();

        this.card.setIcon(cardCover);
        this.card.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                click();
                if(opened.size() == 2){
                    numOfTries += 1;
                    tries.setText(String.valueOf(numOfTries));
                    timer.start();
                }
            }
        });
        timer.setRepeats(false);
    }

    public void loadCardImage() {
        // mengubah gambar tombol kartu menjadi yang di inginkan
        try {
            BufferedImage image = ImageIO.read(new File("D:\\UI\\Semester 2\\DDP-2\\TugasProgDDP2\\" +
                    "assignment-4\\images\\cardCover.jpg"));
            this.cardCover = new ImageIcon(image);
            image = ImageIO.read(new File("D:\\UI\\Semester 2\\DDP-2\\TugasProgDDP2\\assignment-4\\" +
                    "images\\card" + cardId + ".jpg"));
            this.cardPicture = new ImageIcon(image);
            cardArrayList.add(this);
        }
        catch (IOException e) {
            System.out.println(e);
            System.out.println(cardId);
        }
    }

    public JButton getCard() {
        return card;
    }

    public Object getCardId() {
        return cardId;
    }

    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }

    public void click() {
        if(this.clicked == false){
            this.card.setIcon(cardPicture);
            clicked = true;
            opened.add(this);
        }
        else{
            this.card.setIcon(cardCover);
            clicked = false;
            opened.clear();
        }
    }

    public void reset(String cardId) {
        // untuk restrart game
        this.cardId = cardId;
        this.setEnabled(true);
        matchedCounter = 0;
        if (this.isMatched()) {
            click();
            this.setMatched(false);
            loadCardImage();
        } else {
            loadCardImage();
        }

        this.getCard().setEnabled(true);
        this.clicked = false;
    }

    public static void resetAll(){
        // untuk restrat number of tries
        numOfTries = 0;
        tries.setText("0");
        for (int i = 0; i < opened.size(); i++) {
            opened.get(i).setClicked(false);
        }
    }

    public static JLabel getTries() {
        tries =  new JLabel("0");
        return tries;
    }
}